$(document).ready(function () {
    includeHTML();
    addanimatedchecker();
    AOS.init();
    countingup();
    countingdown();
    scrollFunction();
});

window.addEventListener('load', function () {
    AOS.refresh();
});

$(window).resize(function () {
    AOS.refresh();
});





/*  includes old functions  */

/**
 * 1.0 - Back to Top + Fix Top Submenu
 */
// When the user scrolls down 96px from the top of the document, show the button
function scrollFunction() {
    if (document.body.scrollTop > 96 || document.documentElement.scrollTop > 96) {
        $("#myBtn").show();
        $(".active .dropdownmenu").addClass('drop');
    } else if (document.body.scrollTop < 96 || document.documentElement.scrollTop < 96) {
        $("#myBtn").hide();
        $(".active .dropdownmenu").removeClass('drop');
    } else {
    }
}
$(window).on("scroll load", function () {
    scrollFunction();
});

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    $('html,body').animate({ scrollTop: 0 }, 'slow');
}

/**
 * 2.0 - Add Active Class to Submenu of Current Page
 */
function setNavigation() {
    var path = window.location.pathname;
    path = path.substring(path.lastIndexOf('/') + 1);

    $(".nav-item a").each(function () {
        var href = $(this).attr('href');
        if (path === href) {
            // $(this).closest('li').addClass('active');
        }
    });
}

$(window).on('scroll', function () {
    setNavigation();
});

/**
 * 2.5 - Set Language Link
 */

function setLangURL() {
    var path = window.location.pathname;
    path = path.substring(path.lastIndexOf('/') + 1);

    $(".lang-link").each(function () {

        if ($(this).hasClass("en")) {
            document.getElementById("langlinkEN").href = "../zh/" + path;
        } else if ($(this).hasClass("zh")) {
            document.getElementById("langlinkZH").href = "../en/" + path;
        }
    });
}

$(window).on('scroll', function () {
    setLangURL();
});

$(window).on('load', function () {
    setLangURL();
});


/**
 * 3.0 - Smooth Scrolling
 */
$(window).on("load", function () {
    // Add smooth scrolling to all links
    $(".active a.menu2").on('click', function (event) {

        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
            // Prevent default anchor click behavior
            event.preventDefault();

            // Store hash
            var hash = this.hash;

            // Using jQuery's animate() method to add smooth page scroll
            // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
            $('html, body').animate({
                scrollTop: $(hash).offset().top
            }, 800, function () {

                // Add hash (#) to URL when done scrolling (default click behavior)
                window.location.hash = hash;
            });
        } // End if
    });
});

/**
 * 4.0 - includeHTML
 */
function includeHTML() {
    var z, i, elmnt, file, xhttp;
    /* Loop through a collection of all HTML elements: */
    z = document.getElementsByTagName("*");
    for (i = 0; i < z.length; i++) {
        elmnt = z[i];
        /*search for elements with a certain atrribute:*/
        file = elmnt.getAttribute("include-html");
        if (file) {
            /* Make an HTTP request using the attribute value as the file name: */
            xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function () {
                if (this.readyState == 4) {
                    if (this.status == 200) { elmnt.innerHTML = this.responseText; }
                    if (this.status == 404) { elmnt.innerHTML = "Page not found."; }
                    /* Remove the attribute, and call this function once more: */
                    elmnt.removeAttribute("include-html");
                    includeHTML();
                }
            }
            xhttp.open("GET", file, true);
            xhttp.send();
            /* Exit the function: */
            return;
        }
    }
}

/**
 * 5.0 - RunningNumberEffect
 */

$(window).on('scroll', function () {
    countingup();
    countingdown();
    addanimatedchecker();
});

function countingup() {
    $('.countup.animatedchecker.aos-animate').each(function () {
        $(this).prop('Counter', 0).animate({
            Counter: $(this).data('id')
        }, {
            duration: 2500,
            easing: 'swing',
            step: function () {
                var num = Math.floor(this.Counter).toString();
                if (Number(num) > 999) {
                    while (/(\d+)(\d{3})/.test(num)) {
                        num = num.replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
                    }
                }
                $(this).text(num);
            },
            complete: function () {
                var num = Math.floor(this.Counter).toString();
                if (Number(num) > 999) {
                    while (/(\d+)(\d{3})/.test(num)) {
                        num = num.replace(/(\d+)(\d{3})/, '$1' + ',' + '$2');
                    }
                }
                $(this).text(num);
            }
        });
        $(this).removeClass("animatedchecker");
    });
}

function countingdown() {
    $('.countdown.animatedchecker.aos-animate').each(function () {
        $(this).prop('Counter', $(this).data('id')).animate({
            Counter: 0
        }, {
            duration: 2500,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
        $(this).removeClass("animatedchecker");
    });
}

/* Add Class animatedchecker when view is above the element */

function addanimatedchecker() {
    $('.countup').each(function () {
        if ($(this).hasClass("aos-animate")) {
        } else {
            $(this).addClass("animatedchecker");
            $(this).text(0);
        }
    });
    $('.countdown').each(function () {
        if ($(this).hasClass("aos-animate")) {
        } else {
            $(this).addClass("animatedchecker");
        }
    });
}